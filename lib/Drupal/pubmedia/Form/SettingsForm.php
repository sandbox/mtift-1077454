<?php
/**
* @file
* Contains \Drupal\pubmedia\Controller\PubmediaController.
*/

namespace Drupal\pubmedia\Form;

use Drupal\system\SystemConfigFormBase;

/**
* Provides a test form object.
*/
class PubmediaSettingsForm implements SystemConfigFormBase {

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormID() {
    return 'pubmedia_options';
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state) {

    $config = config('pubmedia_npr_api_key');

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => t("Please enter your station's NPR API key"),
      '#default_value' => $config->get('enabled'),
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::validateForm().
   */
  public function validateForm(array &$form, array &$form_state) {
    drupal_set_message(t('The FormTestObject::validateForm() method was used for this form.'));
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state) {
    config('system.maintenance')
      ->set('enabled', $form_state['values']['maintenance_mode'])
      ->set('message', $form_state['values']['maintenance_mode_message'])
      ->save();

    parent::submitForm($form, $form_state);
  }


}
