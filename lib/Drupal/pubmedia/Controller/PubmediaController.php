<?php
/**
* @file
* Contains \Drupal\pubmedia\Controller\PubmediaController.
*/

namespace Drupal\pubmedia\Controller;

use Drupal\Core\Controller\ControllerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
* Controller routines for pubmedia routes.
*/
class PubmediaController implements ControllerInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * Constructs a PubmediaController object.
   */
  public function __construct() {
  }

  /**
   * Returns an administrative overview of all public media services.
   *
   * @return string
   *   A HTML-formatted string with the administrative page content.
   *
   */
  public function adminOverview() {
  }

  /**
   * Prints a listing of all books.
   *
   * @return string
   *   A HTML-formatted string with the listing of all recent content.
   */
  public function pubmediaRender() {

    $client = \Drupal::httpClient()->setBaseUrl('http://api.npr.org');
    $request = $client->get('query?orgId=XXXX&apiKey=XXXXX');
    $response = $request->send();

    return ($response);

  }

}

